# Generate i18n Rede Michelin

JAR gerador de arquivos json com traduções utilizadas pelo front-end para plataforma *PIMI - Portal Rede Michelin*, versão PHP.

### Features
  - Gerar arquivo JSON, baseado em tabela TRANSLATION
  - Os idiomas manipulados são PT, EN, ES e FR (Padrão PIMI)
  - Path para criação de arquivos via resource

### Resource

Em arquivo properties é configurado banco oracle e diretório que determina os JSONs serão criados.

| Variável | Descrição |
| ------ | ------ |
| json.path.create | Diretório de criação de JSONs |
| jdbc.driver | Driver de banco |
| jdbc.url | String de conexão |
| jdbc.user | Login de credencial |
| jdbc.password | Senha de credencial |

Made by
----
[Renatão Alves](https://www.linkedin.com/in/paulo-renato/)

**Conhecimento deve ser compartilhado e não ilhado.**