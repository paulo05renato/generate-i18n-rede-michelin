package database;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class SQLConnectionManager {

	private static SQLConnectionManager instance;
	private ComboPooledDataSource cpds;

	private String url;
	private String user;
	private String password;
	private String driver;
	private String minPoolSize;
	private String maxPoolSize;
	private String incrementPoolSize;

	public ResourceBundle getBundle() {
		return ResourceBundle.getBundle("pimi");
	}

	public String getKeyString(String key) {
		return getBundle().getString(key);
	}

	private SQLConnectionManager() {

		cpds = new ComboPooledDataSource();

		try {

			driver = getKeyString("jdbc.driver");

			Class.forName(driver);

			url = getKeyString("jdbc.url");
			user = getKeyString("jdbc.user");
			password = getKeyString("jdbc.password");
			minPoolSize = getKeyString("c3po.MinPoolSize");
			maxPoolSize = getKeyString("c3po.MaxPoolSize");
			incrementPoolSize = getKeyString("c3po.AcquireIncrement");

			cpds.setDriverClass(driver);
			cpds.setJdbcUrl(url);
			cpds.setUser(user);
			cpds.setPassword(password);

			cpds.setMinPoolSize(Integer.parseInt(minPoolSize));
			cpds.setAcquireIncrement(Integer.parseInt(incrementPoolSize));
			cpds.setMaxPoolSize(Integer.parseInt(maxPoolSize));

		} catch (PropertyVetoException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.out.println("ERROR: Error loading JDBC driver. Class " + driver + " not found.");
		}

	}

	public static SQLConnectionManager getInstance() {
		if (instance == null)
			instance = new SQLConnectionManager();
		return instance;
	}

	public Connection getConnection() throws SQLException {
		return cpds.getConnection();
	}

}