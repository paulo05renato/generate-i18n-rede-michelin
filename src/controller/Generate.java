package controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

import dao.TranslationDAO;
import database.SQLConnectionManager;
import model.Translation;
import model.enums.Language;

public class Generate {

	public void build() {

		Map<Language, String> map = createMap();

		for (Language lang : map.keySet()) {
			boolean isStatus = createFile(lang.getValue(), map.get(lang));

			if (isStatus)
				System.out.println("File: " + lang.getValue() + ".json created !!");
		}

	}

	private Map<Language, String> createMap() {
		Map<Language, String> map = new HashMap<Language, String>();

		TranslationDAO instance = TranslationDAO.getInstance();

		map.put(Language.PT_BR, toJson(instance.ptBR()));
		map.put(Language.EN_US, toJson(instance.enUS()));
		map.put(Language.ES_ES, toJson(instance.esES()));
		map.put(Language.FR_FR, toJson(instance.frFR()));

		return map;
	}

	private String toJson(List<Object> objects) {

		Map<String, Map<String, String>> map = new HashMap<String, Map<String, String>>();

		objects.stream().forEach(object -> {
			Translation item = (Translation) object;

			String group = item.getIdTranslationGroup();
			if (!map.containsKey(group))
				map.put(group, new HashMap<String, String>());

			String key = item.getIdTranslationKey();
			if (!map.get(group).containsKey(key))
				map.get(group).put(key, item.getVlTranslation());

		});

		return (new Gson()).toJson(map);
	}

	private boolean createFile(final String filename, final String content) {

		final String PATH = SQLConnectionManager.getInstance().getKeyString("json.path.create");

		try {
			File directory = new File(PATH);

			if (!directory.exists())
				directory.mkdirs();

			File file = new File(PATH + filename + ".json");
			file.createNewFile();

			OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
			osw.write(content);
			osw.flush();
			osw.close();

			return true;

		} catch (IOException e) {
			e.printStackTrace();
		}

		return false;

	}

}
