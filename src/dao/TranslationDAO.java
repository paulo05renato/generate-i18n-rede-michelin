package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import dao.abstracts.BaseDAO;
import model.Translation;

public class TranslationDAO extends BaseDAO {

	private static TranslationDAO instance;

	public static TranslationDAO getInstance() {
		if (instance == null)
			instance = new TranslationDAO();
		return instance;
	}

	public TranslationDAO() {
		this.tableName = new String("translation");
	}

	public List<Object> ptBR() {
		return list("pt-BR");
	}

	public List<Object> enUS() {
		return list("en-US");
	}

	public List<Object> esES() {
		return list("es-ES");
	}

	public List<Object> frFR() {
		return list("fr-FR");
	}

	private List<Object> list(String idLanguage) {
		return getList("SELECT * FROM " + this.tableName
				+ " WHERE id_language = ? ORDER BY id_translation_group, id_translation_key ", idLanguage);
	}

	protected Translation processRow(ResultSet rs) throws SQLException {
		Translation object = new Translation();

		object.setIdTranslationGroup(rs.getString("id_translation_group"));
		object.setIdTranslationKey(rs.getString("id_translation_key"));
		object.setVlTranslation(rs.getString("vl_translation"));
		object.setIdLanguage(rs.getString("id_language"));

		return object;
	}
}