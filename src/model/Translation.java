package model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Translation {

	private String idTranslationGroup;
	private String idTranslationKey;
	private String idLanguage;
	private String vlTranslation;

}