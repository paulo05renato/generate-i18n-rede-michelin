package model.enums;

import lombok.Getter;

@Getter
public enum Language {

	PT_BR("pt-BR"), EN_US("en-US"), ES_ES("es-ES"), FR_FR("fr-FR");

	private String value;

	Language(String value) {
		this.value = value;
	}

}
